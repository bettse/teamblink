
import React from 'react'
import reactCSS from 'reactcss'

const color = require('react-color/lib/helpers/color').default
const { ColorWrap, EditableInput } = require('react-color/lib/components/common')
import BlockLabelSwatches from './BlockSwatches'

export const BlockWithLabel = ({ onChange, hex, colors, width, triangle }) => {
  const handleChange = (hexCode) => {
    if (color.isValidHex(hexCode)) {
      onChange({
        hex: hexCode,
        source: 'hex'
      })
    }
  }

  const styles = reactCSS({
    default: {
      card: {
        width,
        background: '#fff',
        boxShadow: '0 1px rgba(0,0,0,.1)',
        borderRadius: '6px',
        position: 'relative'
      },
      head: {
        height: '110px',
        background: hex,
        borderRadius: '6px 6px 0 0',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      },
      body: {
        padding: '10px'
      },
      label: {
        fontSize: '18px',
        color: '#fff'
      },
      triangle: {
        width: '0px',
        height: '0px',
        borderStyle: 'solid',
        borderWidth: '0 10px 10px 10px',
        borderColor: `transparent transparent ${hex} transparent`,
        position: 'absolute',
        top: '-10px',
        left: '50%',
        marginLeft: '-10px'
      },
      input: {
        width: '100%',
        fontSize: '12px',
        color: '#666',
        border: '0px',
        outline: 'none',
        height: '22px',
        boxShadow: 'inset 0 0 0 1px #ddd',
        borderRadius: '4px',
        padding: '0 7px',
        boxSizing: 'border-box'
      }
    },
    'hide-triangle': {
      triangle: {
        display: 'none'
      }
    }
  }, { 'hide-triangle': triangle === 'hide' })

  return (
    <div style={styles.card} className='block-picker'>
      <div style={styles.triangle} />

      <div style={styles.head}>
        <div style={styles.label}>
          { hex }
        </div>
      </div>

      <div style={styles.body}>
        <BlockLabelSwatches colors={colors} onClick={handleChange} />
        <EditableInput
          placeholder='Hex Code'
          style={{ input: styles.input }}
          value=''
          onChange={handleChange}
        />
      </div>
    </div>
  )
}

BlockWithLabel.defaultProps = {
  width: '170px',
  colors: ['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',
           '#dce775', '#ff8a65', '#ba68c8'],
  triangle: 'top'
}

BlockWithLabel.propTypes = {
  onChange: React.PropTypes.func,
  hex: React.PropTypes.string,
  colors: React.PropTypes.arrayOf(
    React.PropTypes.oneOfType([
      React.PropTypes.object,
      React.PropTypes.string
    ]),
  ),
  width: React.PropTypes.string,
  triangle: React.PropTypes.string
}

export default ColorWrap(BlockWithLabel); // eslint-disable-line
