import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import NavigationClose from 'material-ui/svg-icons/navigation/close'

import Paper from 'material-ui/Paper'
import { Card, CardTitle } from 'material-ui/Card'
import { ipcRenderer } from 'electron'
import { connect } from 'mqtt'

import { BlockWithLabel } from './Block'
import './App.css'
import config from './config'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      client: null,
      error: '',
      blinks: [],
      mySerial: ''
    }
  }

  componentDidMount () {
    window.fetch(config.feedsUrl, {
      headers: {
        'X-AIO-Key': config.apiKey
      }
    }).then((response) => {
      return response.json().then((feeds) => {
        const blinks = feeds.sort((a, b) => { return a.description.toLowerCase().localeCompare(b.description.toLowerCase()) })
        const client = connect(config.adafruitWebsocket, {
          username: config.username,
          password: config.apiKey
        })

        client.on('connect', this.handleMqttConnect.bind(this))
        client.on('error', this.handleMqttError.bind(this))
        client.on('message', this.handleMqttMessage.bind(this))
        ipcRenderer.on('blink', this.handleIpcEvent.bind(this))
        this.setState({ client, blinks })
      })
    })
  }

  // Takes a string, determines the version, returns the color
  /*
  {
    "version": 0,
    "color": "#00FFFF"
  }
  */
  extractColorFromMessage (message) {
    if (message[0] === '#') { // pre-json version
      console.log('Non-json message')
      return message
    }
    let parsed = JSON.parse(message)
    console.log('Message version', parsed.version)
    switch (parsed.version) {
      case 0:
        return parsed.color
      default:
        return '#000000'
      // No other versions known at this time
    }
  }

  handleIpcEvent (event, message) {
    const { blinks } = this.state
    let sn = message.toLowerCase()
    console.log('Got serial number', sn)
    const blink = blinks.find((b) => { return (b.key.slice(-8).toLowerCase() === sn) })

    if (blink) {
      console.log('Sending current color to blink')
      ipcRenderer.send('blink', this.extractColorFromMessage(blink.last_value))
    }

    this.setState({ mySerial: sn })
  }

  handleMqttConnect () {
    const { blinks, client } = this.state
    const topics = blinks.map((blink) => {
      return `${config.username}/feeds/${blink.key}`
    })

    client.subscribe(topics)
    client.subscribe(`${config.username}/errors`)
    client.subscribe(`${config.username}/throttle`)
  }

  handleMqttMessage (topic, payload) {
    const message = payload.toString()
    if (topic === `${config.username}/errors`) {
      this.handleMqttError(message)
    } else if (topic === `${config.username}/throttle`) {
      this.handleMqttError(message)
    } else {
      const { blinks, mySerial } = this.state
      const key = topic.split('/').pop()
      const blink = blinks.find((b) => { return b.key === key })

      blink.last_value = message

      if (key.slice(-8).toLowerCase() === mySerial) {
        ipcRenderer.send('blink', this.extractColorFromMessage(message))
      }

      this.setState({ blinks })
    }
  }

  handleMqttError (error) {
    this.setState({ error })
  }

  navigationClose (event) {
    ipcRenderer.send('quit')
  }

  handleChangeComplete (color, blinkIndex) {
    const { client, blinks } = this.state
    const blink = blinks[blinkIndex]
    const { key } = blink
    // Existing clients cannot handle json message format, so we cannot use it yet.
    client.publish(`${config.username}/feeds/${key}`, color.hex)
  }

  render () {
    const { defaultColor, colors } = this.props
    const { blinks, error } = this.state
    return (
      <MuiThemeProvider>
        <div>
          <AppBar title='Team Blinks'
            iconElementLeft={<IconButton><NavigationClose /></IconButton>}
            onLeftIconButtonTouchTap={this.navigationClose}
          />
          <Paper className='paper' zDepth={1}>
            {blinks.map((blink, index) => (
              <Card className='card' key={index}>
                <CardTitle title={blink.description} />
                <div className='picker'>
                  <BlockWithLabel colors={colors} hex={this.extractColorFromMessage(blink.last_value) || defaultColor} onChange={(color) => { this.handleChangeComplete(color, index) }} />
                </div>
              </Card>
            ))}
            {error.length > 0 && <Card className='error' zDepth={5}> {error} </Card>}
          </Paper>
        </div>
      </MuiThemeProvider>
    )
  }
}

App.defaultProps = {
  defaultColor: '#000',
  colors: [
    { hex: '#000000', label: 'Off' },
    { hex: '#00FFFF', label: 'Aqua team!' },
    { hex: '#0099cc', label: 'Question' },
    { hex: '#FF4040', label: 'Disagree/Lost' },
    { hex: '#37D67A', label: 'Following/Agree' },
    { hex: '#ffe700', label: 'Mixed/Unknown' },
    { hex: '#ba68c8' },
    { hex: '#eeeeee' }
  ]
}

App.propTypes = {
  defaultColor: React.PropTypes.string,
  colors: React.PropTypes.arrayOf(React.PropTypes.object)
}

export default App
