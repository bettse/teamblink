var menubar = require('menubar')
var electron = require('electron')
var AutoLaunch = require('auto-launch')
var Blink1 = require('node-blink1')
var usbDetect = require('usb-detection')
var tinycolor = require('tinycolor2')
var config = require('./config')

const BLINK_VENDOR_ID = config.blink1.vendor_id
const BLINK_PRODUCT_ID = config.blink1.product_id
const isDev = (process.env.NODE_ENV === 'development')

var app = electron.app
var ipcMain = electron.ipcMain
var blink1

let prodOptions = {
  dir: app.getAppPath() + '/app/build/',
  width: 575,
  resizable: false,
  preloadWindow: true
}

let devOptions = {
  index: 'http://localhost:3000',
  dir: app.getAppPath() + '/app/build/',
  width: 575,
  preloadWindow: true,
  alwaysOnTop: true
}

var mb = menubar(isDev ? devOptions : prodOptions)
mb.on('ready', () => {})
mb.on('after-create-window', postCreateWindow)
mb.on('after-close', postClose)

function postCreateWindow () {
  if (isDev) {
    mb.window.openDevTools()
  }
  usbDetect.find(BLINK_VENDOR_ID, BLINK_PRODUCT_ID, findBlink)
  usbDetect.on(`add:${BLINK_VENDOR_ID}:${BLINK_PRODUCT_ID}`, blinkConnected)
  usbDetect.on(`remove:${BLINK_VENDOR_ID}:${BLINK_PRODUCT_ID}`, blinkDisconnected)
  autoLauncher.isEnabled().then((currentState) => {
    mb.window.webContents.send('autostart', currentState)
  })
}

function postClose () {
  console.log('Stopping usb detection')
  usbDetect.stopMonitoring()
}

function findBlink (err, devices) {
  if (err) {
    console.log('Error during usbDetect.find', err)
    return
  }
  console.log('Found', devices.length, 'devices')
  devices.forEach(blinkConnected)
}

function blinkConnected (device) {
  if (blink1) { // Blink already found
    return
  }
  console.log('Blink connected', device.serialNumber)
  try {
    blink1 = new Blink1(device.serialNumber)
  } catch (e) {
    console.log('Error with blink', device.serialNumber)
  }
  setTimeout(() => {
    mb.window.webContents.send('blink', blink1.serialNumber.toLowerCase())
  }, 2000)
}

function blinkDisconnected (device) {
  blink1 = null
  mb.window.webContents.send('blink', '')
}

ipcMain.on('quit', (event) => { app.quit() })
ipcMain.on('blink', incomingEvent)

function incomingEvent (event, arg) {
  console.log('ipcMain', 'blink', arg)
  if (blink1) {
    var tc = tinycolor(arg)
    var color = tc.toRgb()
    blink1.setRGB(color.r, color.g, color.b)
  }
}

var autoLauncher = new AutoLaunch({
  name: 'TeamBlink'
})

ipcMain.on('autostart', (event, newState) => {
  if (newState) {
    autoLauncher.enable()
  } else {
    autoLauncher.disable()
  }
})
